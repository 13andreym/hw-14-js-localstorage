document.addEventListener("DOMContentLoaded", function () {
        const btn = document.querySelector(".btn");

       
        const savedTheme = localStorage.getItem("theme");
        if (savedTheme) {
          document.body.classList.add(savedTheme);
        }

        
        btn.addEventListener("click", function () {
          document.body.classList.toggle("dark-theme");

          
          const currentTheme = document.body.classList.contains("dark-theme")
            ? "dark-theme"
            : "";
          localStorage.setItem("theme", currentTheme);
        });
      });
